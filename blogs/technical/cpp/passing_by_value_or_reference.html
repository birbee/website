<!DOCTYPE html>
<html lang="en-GB">
<head>
<title>Passing by value or reference | cppocl</title>
<link rel="stylesheet" href="styles.css">
</head>
<body>
<div class="form">
<form action="cpp_blogs.html">
    <input type="submit" value="C++ blogs">
</form>
</div>
<div class="blog">
<h1>Taking a look at passing by reference or value</h1>
Deciding when to pass a variable by value or pass a variable by reference can make a significant difference in the performance of the software.
If a function call is being made millions of times within a tight loop, making the correct decision will allow the software to run faster, saving time.

This might not always be measurable, but even a decision early on can lead into a performance problem later as the source code grows in size, and these inefficient functions become used more often.

Experience tells you when you want to pass by value or pass by reference, but in reality a simple explanation can help people go a long way.
<h1>A quick comparison of a variable and a reference to the variable</h1>
Notice that the reference variable r is initialized to i. Changing either variable will change the value of i.

<pre class="code">
int main()
{
    int i = 1;

    // r references i. Changing r will change i.
    int & r = i;

    // cr references i but cannot change i through cr.
    // Changing i or r will allow cr to see the changed value.
    int const & cr = i;

    // i has changed to 2.
    r = 2;

    return 0;
}
</pre>

<h1>A quick overview of pass by value</h1>
Any value passed into a function that is implemented using pass by value will take a copy of the original value, and will not change the passed in value outside the function call.

Here is a simple code example that will demonstrate pass by value:
<pre class="code">
using std::cout;
using std::endl;

void f1(int n)
{
    cout &#60;&#60; "n = " &#60;&#60; n &#60;&#60; endl;
    n++;
    cout &#60;&#60; "n = " &#60;&#60; n &#60;&#60; endl;
}

int main()
{
    int x = 1;
    cout &#60;&#60; "f1(" &#60;&#60; x &#60;&#60; ")" &#60;&#60; endl;
    f1(x);
    cout &#60;&#60; "x = " &#60;&#60; x &#60;&#60; endl;
    return 0;
}
</pre>

and the output...
<pre>
f1(1)
n = 1
n = 2
x = 1
</pre>
Notice that the value has changed within function f1, but has not been affected outside the call to f1.
<h1>A quick overview of pass by reference</h1>
Any value passed into a function that is implemented using a reference will have access to the data at the same memory location, and will change the value passed from out outside the function call.

Here is a simple code example that will demonstrate pass by reference:
<pre class="code">
void f2(int & n)
{
    cout &#60;&#60; "n = " &#60;&#60; n &#60;&#60; endl;
    n++;
    cout &#60;&#60; "n = " &#60;&#60; n &#60;&#60; endl;
}

int main()
{
    int x = 1;
    cout &#60;&#60; "f2(" &#60;&#60; x &#60;&#60; ")" &#60;&#60; endl;
    f2(x);
    cout &#60;&#60; "x = " &#60;&#60; x &#60;&#60; endl;
    return 0;
}
</pre>
and the output...
<pre>
f2(1)
n = 1
n = 2
x = 2
</pre>
Notice that the value has changed within function f2, and the value retains its changed value outside the call to f2.
<h1>Expanding on the explanation</h1>
The difference may not seem important, but what happens when that variable being passed by value is large data like a list?
<pre class="code">
void f(list&#60;string&#62; items)
{
// ...
}

int main()
{
    list&#60;string&#62; items;
    // fill items from database...
    f(items);
    return 0;
}
</pre>

All the strings within the items list will be copied when passed into function f.
Although this is obvious to many experienced people, this kind of mistake is commonly found within source code, and can make a very big difference in the performance of the software.
Of course a simple change can make a big difference, especially when implemented consistently.
<pre class="code">
void f(const list&#60;string&#62; &amp; items)
{
// ...
}
</pre>

Now using function f will no long cause a possible performance issue as the variable reference is only passed to the function.

You might wonder why using a reference might be better than using a pointer?
The problem with passing pointers into functions is this can lead to confusion about who is checking the pointer is valid (i.e. not null).
Many software problems occur by trying to use a pointer that is null or uninitialized, and these problems can be avoided by using a reference.
Pointers have their uses, they can be better than using a reference if you want a value that may or may not need using, so the pointer could be set to null to indicate it's not required.

<h1>Useful strategies for deciding when to use a pointer, reference or passing by value</h1>
Here are some common behaviors desired and the most suitable solution:
<table>
<tr>
<th>function behavior</th><th>value,<br/>reference<br/>or<br/>pointer</th><th>code example</th>
</tr>
<tr>
<td>re-use parameter but don&#39;t return to caller</td><td>value</td><td><span class="code">void f(int value)<br/>&#123;<br/>&#125;</span></td>
</tr>
<tr>
<td>change parameter and return to caller</td><td>reference</td><td><span class="code">void f(string & value)<br/>&#123;<br/>&#125;</span></td>
</tr>
<tr>
<td>use parameter without changing value</td><td>const reference</td><td><span class="code">void f(string const & value)<br/>&#123;<br/>&#125;</span></td>
</tr>
<tr>
<td>use parameter without changing value for primitive type</td><td>const</td><td><span class="code">void f(int const value)<br/>&#123;<br/>&#125;</span></td>
</tr>
<tr>
<td>optionally change parameter and return to caller</td><td>pointer</td><td><span class="code">void f(int * value)<br/>&#123;<br/>&nbsp;&nbsp;if (value) *value = 1;<br/>&#125;</span></td>
</tr>
<tr>
<td>optionally use parameter and don&#39;t return to caller</td><td>const pointer</td><td><span class="code">void f(int const * value)<br/>&#123;<br/>&nbsp;&nbsp;if (value)<br/>&#125;</span></td>
</tr>
<tr>
<td>allocate memory and return to caller</td><td>pointer reference</td><td><span class="code">void f(int * & value)<br/>&#123;<br/>&nbsp;&nbsp;value = new int[10];<br/>&#125;</span></td>
</tr>
</table>

<h1>The obsession of constants</h1>
The example of a constant primitive type is not actually necessary, and also prevents someone from re-using the passed in value, which could be useful or even desirable.
Decide when making something constant and a reference is desirable, but if the variable could be re-used or needs to be copied internally then it might not make sense to make the parameter of the function constant or a reference.
</div>
</body>
