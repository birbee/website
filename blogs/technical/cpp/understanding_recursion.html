<!DOCTYPE html>
<html lang="en-GB">
<head>
<title>Understanding recursion | cppocl</title>
<link rel="stylesheet" href="styles.css">
</head>
<body>
<div class="form">
<form action="cpp_blogs.html">
    <input type="submit" value="C++ blogs">
</form>
</div>
<div class="blog">
<h1>What is recursion?</h1>
In programming terms this is a function that will end up calling itself again, possibly many times until an exit condition is reached.
The exit condition will ensure the function no longer needs to call itself, allowing the function to eventually unwind to the point of the initial call.

The following common example demonstrates a recursive function:
<pre class="code">
int factorial(int n)
{
    if (n > 1)
        n *= factorial(n - 1);
    return n;
}
</pre>

If we want to calculate a factoring for 3 (i.e. 3*2*1) then we just call the factorial function with 3, as demonstrated:
<pre class="code">
int main()
{
    int n = factorial(3);

    // output result which will be 6.
    std::cout << n << std::endl;
}
</pre>

<h1>Understanding the inner workings of a recursive function</h1>
Looking at the recursive function above, it might seem confusing at first how this works.
Lets look at the given example in terms of each call to the factorial function:

<pre class="note">
Initial call to recursive function:
</pre>
<pre class="faint_code">
int main()
{
    <b>int n = factorial(3);</b>
    std::cout << n << std::endl;
}
</pre>
<pre class="note">First time inside the function<br/>n is 3, so the function will call itself</pre>
<pre class="faint_code">
int factorial(int n)
{
    if (n > 1)
        <b>n *= factorial(n - 1);</b>
    return n;
}
</pre>
<pre class="note">After call to self with n - 1, n is now 2,<br/>which will result in another call to itself</pre>
<pre class="faint_code">
int factorial(int n)
{
    if (n > 1)
        <b>n *= factorial(n - 1);</b>
    return n;
}
</pre>
<pre class="note">Within final call to self, n is now 1.<br/>This means no more calls to self and n is returned</pre>
<pre class="faint_code">
int factorial(int n)
{
    if (n > 1)
        n *= factorial(n - 1);
    <b>return n;</b>
}
</pre>
<pre class="note">After the last call to itself returns 1,<br/>we end up back in our own function again,<br/>but with n now 2 as we are now unwinding.<br/>n *= function return will complete.</pre>
<pre class="faint_code">
int factorial(int n)
{
    if (n > 1)
        <b>n *= </b>factorial(n - 1);
    return n;
}
</pre>
<pre class="note">After n *= function call is complete,<br/>n is returned (which will be 2).</pre>
<pre class="faint_code">
int factorial(int n)
{
    if (n > 1)
        n *= factorial(n - 1);
    <b>return n;</b>
}
</pre>
<pre class="note">After the call again to itself returns 2,<br/>we end up back in our own function again,<br/>but with n now 3 as we continue unwinding.<br/>n *= function return will complete.</pre>
<pre class="faint_code">
int factorial(int n)
{
    if (n > 1)
        <b>n *= </b>factorial(n - 1);
    return n;
}
</pre>
<pre class="note">After n *= function call is complete,<br/>n is returned (which will be 3 * 2 = 6).</pre>
<pre class="faint_code">
int factorial(int n)
{
    if (n > 1)
        n *= factorial(n - 1);
    <b>return n;</b>
}
</pre>
<pre class="note">After the final unwinding of the last function call we are back in the main function<br/>n will be set to 6 and output to console.</pre>
<pre class="faint_code">
int main()
{
    <b>int n = </b>factorial(3);

    // output result which will be 6.
    std::cout << n << std::endl;
}
</pre>

Each time a function calls itself, this will cause all local data including function parameters to be placed on the stack.
Once an exit condition is met, all the recursive calls will be unwound all the way to the first call, and then finally exiting outside the function completely.

<h1>Another example of recursion</h1>
Any situation where a function can call itself at some point, even via other functions before finally calling itself again is recursive.
So if function x calls function y, which calls function z, which calls function x again, then x is a recursive function.

Here is another example of a recursive function within a usable class:
<pre class="code">
class XmlElement
{
public:
    XmlElement()
    {
    }

    XmlElement(std::string const& name,
               std::string const& value)
        : m_name(name)
        , m_value(value)
    {
    }

    void AddChild(XmlElement const& element)
    {
        m_children.push_back(element);
    }

    bool HasChildren() const
    {
        return !m_children.empty();
    }

    // recursive find
    XmlElement* Find(std::string const& name)
    {
        if (name == m_name)
            return this;

        for (iterator pos = m_children.begin();
             pos != m_children.end(); ++pos)
        {
            XmlElement& child = *pos;
            XmlElement* found = child.Find(name);
            if (found != NULL)
                return found;
        }

        return NULL;
    }

private:
    typedef std::list<XmlElement> ElementList;
    typedef ElementList::iterator iterator;

    std::string m_name;
    std::string m_value;
    ElementList m_children;
};

int main()
{
    XmlElement element("a", "1");
    element.AddChild(XmlElement("b", "2"));
    element.Find("b")->AddChild(
                     XmlElement("d", "4"));
    element.Find("d")->AddChild(
                     XmlElement("e", "5"));
    element.AddChild(XmlElement("c", "3"));
}
</pre>

This example creates xml elements in the following structure:
&nbsp;&nbsp;&nbsp;&nbsp;a
&nbsp;&nbsp;&nbsp;&nbsp;a->b
&nbsp;&nbsp;&nbsp;&nbsp;a->b->d
&nbsp;&nbsp;&nbsp;&nbsp;a->b->d->e
&nbsp;&nbsp;&nbsp;&nbsp;a->c

Although the Find function on the XmlElement class is only calling Find on m_children this function is still recursive.

This is a simple example of how to implement a tree structure, and XML elements provide a classic example of this type of structure.

Note this simple example lacks much functionality that would be useful for an actual XML element class, such as the ability to find first/next elements with the same name.

P.S. This code is functional and free to use and extended.

<h1>To summarize</h1>
Recursion has it's place, and with the XML example provided, this is a good candidate of when recursion makes sense to be used.
This is not always the case, and sometimes a simple loop can solve the problem while providing a simpler solution.

If trying to solve a problem with a loop is becoming complicated, then this could be an indicator that recursion might be a better solution, but be aware that the exit conditions need to be considered carefully to allow the recursion to fully unwind.
</div>
</body>
