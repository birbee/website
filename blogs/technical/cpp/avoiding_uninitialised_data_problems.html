<!DOCTYPE html>
<html lang="en-GB">
<head>
<title>Passing by value or reference | cppocl</title>
<link rel="stylesheet" href="styles.css">
</head>
<body>
<div class="form">
<form action="cpp_blogs.html">
    <input type="submit" value="C++ blogs">
</form>
</div>
<div class="blog">
<h1>Using the features the language provides for free</h1>
C++ provides the ability to initialise data, copy data and clean up the data when done, but unfortunately too often what is provided for free is not used, and people decide to use alternative methods for doing these things, often with unexpected side effects.

By making use of constructors (including copy constructors), assignment operators, and destructors, memory can all be initialised to defaults, resources can be freed once the lifetime of the data has ended, and everything can just work.

Problems begin to creep in when people try to find ways of initialising data in one place, by relying on copy/paste or by using alternative means of initialising and freeing resources.
<h1>Code changes, and problems can be introduced</h1>
So why would someone try to circumvent the language features that C++ provides for constructing objects and cleaning up resources?

Lets look at a simple example of a class using a constructor, destructor, get and set function for managing the member variables.

<pre class="note">
Sample code on this page is purely focusing on initialisation issues, so the use of strcpy and strlen is just for the purpose of simplifying code examples.
</pre>

<pre class="code">
class String
{
public:
    String() : m_string(nullptr)
    {
    }

    ~String()
    {
        delete [] m_string;
    }

    const char* get() const
    {
        return m_string;
    }

    void set(const char* str)
    {
        delete [] m_string;
        m_string = new char[strlen(str)+1];
        strcpy(m_string, str);
    }

private:
    char* m_string;
};
</pre>

Like most code, it starts with a simple implementation, then grows into something bigger and more sophisticated as extra functionality is required.
To demonstrate this problem, lets extend this example class above to provide extra capabilities for the users of the class.
We've decided that we want improve our string class by adding the ability to set the string in the constructor, append to the string and store the length and add a function to return the length efficiently.

<pre class="code">
class String
{
public:
    String()
        : m_string(nullptr)
        , m_length(0)
    {
    }

    String(const char* str)
        : m_length(strlen(str))
        , m_string(new char[m_length+1])
    {
        strcpy(m_string, str);
    }

    ~String()
    {
        delete [] m_string;
    }

    const char* get() const
    {
        return m_string;
    }

    void set(const char* rhs)
    {
        delete [] m_string;
        m_length = strlen(rhs);
        m_string = new char[m_length+1];
        strcpy(m_string, rhs);
    }

    void append(const char* str_to_append)
    {
        m_length += strlen(str_to_append);
        char* new_string = new char[m_length+1];
        strcpy(new_string, m_string);
        strcat(new_string, str_to_append);
        delete [] m_string;
        m_string = new_string;
    }

    size_t length() const
    {
        return m_length;
    }

private:
    char* m_string;
    size_t m_length;
};
</pre>

At this point I have introduced a deliberate bug that will cause a crash, due to the initialisation happening in the order the member variables are defined in the class.
Sometimes when bugs appear due to multiple constructors, there might be a temptation to start adding a public initialise function, but this approach can lead to other problems.
Manual initialisation should be avoided, as constructors are guaranteed to be called automatically, and removes the chance for a manual initialisation to be missed.

<h1>Trying to do the right thing</h1>
Knowing any language to an in-depth level takes time, requires reading books or online resources, writing code and stumbling into problems then learning from those mistakes.
Languages like C++ can be more complicated to work with than other languages, so strategies often get devised to help prevent common problems occurring during the development process.

One strategy that is often used is to create a private initialise function that is used for initialisation of all member variables, and possibly even re-used to set member variables in some situations.
Lets look at a typical initialisation strategy you might see.

<pre class="code">
class String
{
public:
    String()
    {
        initialise(nullptr);
    }

    String(const char* str)
    {
        initialise(str);
    }

    ~String()
    {
        delete [] m_string;
    }

    const char* get() const
    {
        return m_string;
    }

    void set(const char* str)
    {
        delete [] m_string;
        initialise(str);
    }

    void append(const char* str_to_append)
    {
        m_length += strlen(str_to_append);
        char* new_string = new char[m_length+1];
        strcpy(new_string, m_string);
        strcat(new_string, str_to_append);
        delete [] m_string;
        m_string = new_string;
    }

    size_t length() const
    {
        return m_length;
    }

private:
    void initialise(const char* str)
    {
        if (str != nullptr)
        {
            m_length = strlen(str);
            m_string = new char[m_length+1];
            strcpy(m_string, str);
        }
        else
        {
            m_length = 0;
            m_string = nullptr;
        }
    }

    char* m_string;
    size_t m_length;
};
</pre>

Now a private function called <code>initialise</code> has been added that can be used for initialising member variables within constructors, and even used for the <code>set</code> function.
The initialisation function solves the problem of removing the duplicated code initialising member variables, but can introduce other problems.
One common problem is using the <code>initialise</code> function without deleting any allocated memory first, causing memory leaks.
Some problems such as memory leaks can be removed by using smart pointers, such as <code>unique_ptr</code>.

<h1>The types of common problems that lead to bugs or worse</h1>

The examples provided only show simple class examples, but in many real life software systems classes can be large, have many member variables, and all sorts of different ways can be invented for initialising data.
Common problems encountered when dealing with uninitialised data are caused by some of the following:
<ul style="list-style-type:decimal;">
<li>Member variable(s) missing from initialiser list.</li>
<li>Initialise function used in place of constructor.</li>
<li>Use of <code>memset</code> instead of using initialiser list.</li>
<li>Memory leaks can be introduced by not freeing memory before using initialise functions.</li>
<li>Resource handles might not be freed before being acquired again, causing handled leaks.</li>
</ul>

The first point can be considered a simple mistake in missing the initialisation of a member variable, and is common to encounter.

The second point can be found where some form of error detection is being combined with initialising data, in an attempt to handle two things in a single function call, but this often introduces more problems than it will solve.

The third point can be seen when trying to use tricks like sizeof(*this) and using memset to set all data to zero, in an attempt to get around the first problem described where member variables were missed in the initialiser list.
This is even more dangerous, as it might not be safe to initialise data in this way, the member variables might be class types that are unsafe to initialise this way, other member variables could be added later which could cause problems, or simply that zero might actually mean something other than an initial state.

The C++ language provides constructors for reasons, and that is to ensuring all data is initialised when objects are constructed and to prevent unnecessary copying of member variables by using initialiser lists.
The problem with using creating your own initialise function is that it will be called after all member variables have been constructed, so each time an object is created all member variables will be set multiple times.
This also means extra care needs to be taken as it's possible to call these initialise functions at any time, and often that can lead to memory leaks or worse.

<h1>Using constructors sensibly</h1>

Thankfully C++ 11 now provides the ability to call a constructor from another constructor (known as delegating constructors), which means one constructor can be added which initialises all data, and other constructors can re-use one primary constructor.

<pre class="code">
class String
{
public:
    String() : String(nullptr)
    {
    }

    String(const char* str)
        : m_length(str != nullptr ?
                   strlen(str) : 0)
        , m_string(m_length > 0 ?
                   new char[m_length + 1] :
                   nullptr)
    {
        if (str != nullptr)
            strcpy(m_string, str);
    }

    virtual ~String()
    {
        delete [] m_string;
    }

private:
    size_t m_length;
    char* m_string;
};
</pre>

<h1>To summarize</h1>
This article is here to help identify some problems that can be experienced from initialisation issues, and to give some ways of solving them in an attempt to develop robust and safe software.
There may be times when using an initialisation function has a useful purpose, but I would recommend it as something to be used independently of initialising data at creation time.
Constructors will be executed automatically when objects are created, and initialising data properly here will always be safer and more efficient than relying on humans manually calling initialise functions, which will always have a chance of being missed or introducing unexpected 
</div>
</body>
